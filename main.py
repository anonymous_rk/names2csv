import json
import csv


"""
I used json file as an example of API because it is much handy to work with Python.
Although, I made a small research on crypto trade special phrases and the overall 
how-it-work process I could only make general names of the tools(I think so, at least). 
But specific names of tools up to Binance and OKEx out my understanding(I called the 
with the names of Basketball teams😁), so the result can be seem a bit odd😅
"""

def json_loader(): # reads json file and takes necessary data by looping
    f = open('sample.json')
    json_data = json.load(f)
    column_items = []
    for i in range(1, len(json_data['cryptocurrency']['tools'])+1):
        for key, value in json_data['cryptocurrency']['tools'][f"item{i}"].items():
            if key == 'names':
                column_items.append(value)

    return column_items



def csv_generator(): # generates csv file using given data
    header = ['unify_name', 'binance_name', 'okex_name']
    values = json_loader()
    csv_file = open('result.csv', 'w')
    writer = csv.writer(csv_file)
    writer.writerow(header)
    writer.writerows(values)

    csv_file.close()
    return csv_file


def pretty_table(): # creates pretty table view of values
    header = ['unify_name', 'binance_name', 'okex_name']
    values = json_loader()
    from prettytable import PrettyTable
    t = PrettyTable(header)
    for row in values:
        t.add_row(row)
    print(t)


pretty_table()

"""
If you call pretty_table() and it gives you this result:
+-------------+-------------------+-----------------------+
|  unify_name |    binance_name   |       okex_name       |
+-------------+-------------------+-----------------------+
|  analytics  | Los Angeles Kings | Golden State Warriros |
| market_stat |   Boston Celtics  |     Denver Nuggets    |
|     spot    |  Toronto Raptors  |   Philadelphia 76ers  |
|    wallet   |   Chicago Bulls   |     Atlanta Hawks     |
+-------------+-------------------+-----------------------+

"""
